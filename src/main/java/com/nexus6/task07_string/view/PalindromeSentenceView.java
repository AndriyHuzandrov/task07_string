package com.nexus6.task07_string.view;

import com.nexus6.task07_string.servicepack.TextProcessor;

public class PalindromeSentenceView extends AbsStreamView {
  void show(){
    textSource
        .sentenceStream()
        .get()
        .filter(s -> s.length() >0)
        .filter(TextProcessor::findPalindrome)
        .forEach(appLogger::info);
  }
}
