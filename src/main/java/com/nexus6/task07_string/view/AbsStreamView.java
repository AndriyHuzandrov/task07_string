package com.nexus6.task07_string.view;

import com.nexus6.task07_string.model.Publishable;
import static com.nexus6.task07_string.UI.MainMenu.mainLog;
import org.apache.logging.log4j.Logger;

abstract class AbsStreamView implements Updatable {
  Publishable textSource;
  Logger appLogger = mainLog;
  public void update(Publishable source) {
    textSource = source;
    show();
  }
  abstract void show();
}
