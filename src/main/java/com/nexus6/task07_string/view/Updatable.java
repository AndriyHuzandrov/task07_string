package com.nexus6.task07_string.view;

import com.nexus6.task07_string.model.Publishable;

public interface Updatable {
  void update(Publishable source);
}
