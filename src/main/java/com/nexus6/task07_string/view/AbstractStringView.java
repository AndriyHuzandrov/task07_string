package com.nexus6.task07_string.view;

import com.nexus6.task07_string.model.Publishable;

abstract class AbstractStringView implements Updatable {
  private String sourceString;

  public void update(Publishable source) {
    sourceString = source.getAsString();
    show();
  }
  abstract void show();
}
