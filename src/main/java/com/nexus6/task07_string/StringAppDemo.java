package com.nexus6.task07_string;

import com.nexus6.task07_string.UI.MainMenu;
import com.nexus6.task07_string.model.TextHolder;

public class StringAppDemo {

  public static void main(String[] args) {
   new MainMenu(new TextHolder()).showLocalizationMenu();
  }
}
