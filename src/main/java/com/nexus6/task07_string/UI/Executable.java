package com.nexus6.task07_string.UI;

@FunctionalInterface
public interface Executable {
  void execute();
}
