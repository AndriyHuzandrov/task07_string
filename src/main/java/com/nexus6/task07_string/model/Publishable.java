package com.nexus6.task07_string.model;

import com.nexus6.task07_string.view.Updatable;
import java.util.function.Supplier;
import java.util.stream.Stream;

public interface Publishable {
  void subscribeObserver(Updatable obs);
  void removeObserver(Updatable obs);
  void notifyObservers();
  Supplier<Stream<String>> wordsStream();
  String getAsString();
  Supplier<Stream<String>> sentenceStream();
}
